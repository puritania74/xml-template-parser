import {Context} from './Context';
import {Scope} from '../Scope';

export class MultipleContext extends Context {


    init() {
        super.init();

    }

    setItemsValue(name) {
        this._itemsName = name;
    }

    getItemsValue() {
        return this._itemsName;
    }

    setKeyValue(name) {
        this._keyName = name;
    }

    getKeyValue() {
        return this._keyName;
    }

    setItemValue(name) {
        this._itemName = name;
    }

    getItemValue() {
        return this._itemName;
    }

    /**
     *
     * @param {XMLModelItem} item
     * @param parentScope
     * @param {number} index
     */
    createScope( item, parentScope, index ) {

        let keyName = this.getKeyValue();
        let itemName = this.getItemValue();

        let scope = new Scope( parentScope, [keyName, itemName] );

        this.executeBinding(item, scope);

        return scope;
    }

    /**
     *
     * @param {XMLModelItem} item
     * @param {Scope} scope
     * @param {number} index
     */
    afterBuildContext(item, scope, index) {
        //Передать локальные значения итерации в значение переменной
        let keyName = this.getKeyValue();
        let itemName = this.getItemValue();
        let itemsName = this.getItemsValue();

        let key;

        if (typeof keyName === 'undefined' || keyName === null) {
            key = index;
        } else {
            key = scope.getLocalProperty(keyName);
        }

        let name = itemsName.concat('.', key);

        scope.setProperty(name, scope.getLocalProperty(itemName));
    }
}