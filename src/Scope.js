export class Scope {

    /**
     *
     * @param {Scope} [parent = null]
     * @param {Array<string>} [localProperties = []]
     */
    constructor( parent = null, localProperties = [] ) {
        /**
         *
         * @type {Scope}
         * @private
         */
        this._parent = parent;
        this._properties = {};
        this._localProperties = localProperties;
    }

    /**
     *
     * @return {Scope|null}
     */
    getParent() {
        return this._parent;
    }

    hasParent() {
        return !!this._parent;
    }

    /**
     *
     * @param {string} propertyName
     * @param propertyValue
     */
    setProperty( propertyName, propertyValue ) {

        let parts = propertyName.split( '.' );
        let baseName = parts.shift();

        if( this._localProperties.indexOf( baseName ) !== -1 ) {
            this.setPathValue(this._properties, propertyName, propertyValue);
        } else {
            if( this.hasParent() ) {
                this.getParent().setProperty( propertyName, propertyValue );
            } else {
                //Это корневая зона
                this.setPathValue(this._properties, propertyName, propertyValue);
            }
        }
    }

    /**
     *
     * @param {string} propertyName
     * @returns {*}
     */
    getLocalProperty(propertyName) {
        return this._properties[propertyName];
    }

    /**
     *
     * @param {Object} target
     * @param {string} path
     * @param value
     */
    setPathValue(target, path, value) {
        let parts = path.split('.');
        let baseName = parts.shift();

        if( parts.length > 0 ) {
            let val = target[ baseName ] = target[ baseName ] || this.createValueContainer(parts[0]);

            for( let i = 0; i < parts.length; i = i + 1 ) {
                let name = parts[ i ];
                if( i === parts.length - 1 ) {
                    val[ name ] = value;
                } else {
                    val = val[ name ] = val[ name ] || this.createValueContainer(parts[i + 1]);
                }
            }

        } else {
            target[ baseName ] = value;
        }

    }

    getProperties() {
        return this._properties;
    }

    /**
     * @protected
     * @param name
     * @return {*}
     */
    createValueContainer(name) {
        return (/^\d+$/.test(name)) ? [] : {}
    }


}