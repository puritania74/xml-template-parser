var path = require('path');
var libraryName = 'xml-template-parser';
var outputFile = libraryName + '.js';

module.exports = {
    entry: './src/index.js',
    output: {
        library: libraryName,
        libraryTarget: "umd",
        path: path.resolve(__dirname, 'dist'),
        filename: outputFile,
        umdNamedDefine: true
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel',
                query: {
                    presets: [ 'es2015', 'stage-0' ]
                },
                debug: true
            }
        ]
    },
    //devtool: "#inline-source-map",
    debug: true
};