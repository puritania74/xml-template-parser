import { SingleContext, MultipleContext } from './Context';
import { ContentBinding } from './ContentBinding';
import { AttributeBinding } from './AttributeBinding';
import { Scope } from './Scope';

export class XMLTemplate {

    /**
     *
     * @param {XMLModel} model
     */
    constructor( model ) {
        this.model = model;
    }

    parse() {
        this.context = new SingleContext();
        this.scan( this.model.getChildren(), this.context );
    }

    /**
     *
     * @param {Array<XMLModelItem>} items
     * @param context
     * @param path
     */
    scan( items, context, path ) {

        if( typeof path === 'undefined' ) {
            path = [];
        }

        items.forEach( item => {
            // let currentContext;

            let currentPath = path.concat( item.getName() );

            //Сканируем атрибуты элемента
            let itemContext = this.scanAttributes( item, context, currentPath );

            //Проверяем биндинг в контенте элемента
            let propertyName = this.getBindingProperty( item.getText() );
            if( propertyName !== null ) {
                //Binding to content
                // let currentContext = new SingleContext( context, path );
                let binding = new ContentBinding( propertyName );
                itemContext.registerBinding( binding );
                // currentContext.registerBinding( binding );
            }

            //Сканируем дочерние элементы
            this.scan( item.getItems(), itemContext, currentPath );

        } );

    }

    getBindingProperty( text ) {
        let name = null;

        if( text ) {
            let matches = text.trim().match( /^{{(.*)}}$/ );
            name = matches ? matches[ 1 ] : null;
        }

        return name;
    }


    /**
     *
     * @param {XMLModel} documentModel
     * @returns {Object}
     */
    run( documentModel ) {
        let rootScope = new Scope();

        let rootModelItem = documentModel.getChildren()[ 0 ]
        let rootContext = this.context.children[ 0 ];
        scanContext( rootContext, rootScope, rootModelItem );


        /**
         *
         * @param {Context} parentContext
         * @param {Scope} parentScope
         * @param {XMLModelItem} modelItem
         */
        function scanContext( parentContext, parentScope, modelItem ) {

            parentContext.children.forEach( context => {
                let modelItems = modelItem.findAllByName( context.getName() );

                modelItems
                    .forEach( ( item, index ) => {
                        let scope = context.createScope( item, parentScope, index );
                        scanContext( context, scope, item );
                        context.afterBuildContext(item, scope, index);
                    } );

            } );

        }

        return rootScope.getProperties();
    }


    /**
     *
     * @param {XMLModelItem} item
     * @param {Context} parent
     * @param {Array<string>} path
     */
    scanAttributes( item, parent, path ) {

        let attributes = item.getAttributes();
        let context;
        let bindings = [];


        Object.keys( attributes ).forEach( attributeName => {

            if( attributeName === 'for' ) {
                let value = attributes[ attributeName ];
                let regexp = /(.*)\s+in\s+(.*)/i;

                let matches = value.match( regexp );

                if( matches ) {
                    context = new MultipleContext( parent, path );

                    let src = matches[ 1 ];
                    let dst = matches[ 2 ];
                    context.setItemsValue( dst );

                    src = src.replace( /\s/g, '' );

                    if( matches = src.match( /\((.*),(.*)\)/ ) ) {
                        //as object
                        context.setKeyValue( matches[ 1 ] );
                        context.setItemValue( matches[ 2 ] );
                    } else {
                        //item of Array
                        context.setItemValue( src );
                    }
                }
            } else if( attributeName.indexOf( 'xmlns:' ) === 0 ) {

            } else {
                let propertyName = this.getBindingProperty( attributes[ attributeName ] );
                if( propertyName ) {
                    bindings.push( new AttributeBinding( attributeName, propertyName ) );
                }
            }
        } );


        if( !context ) {
            context = new SingleContext( parent, path );
        }

        bindings.forEach( binding => context.registerBinding( binding ) );

        context.setName( item.getName() );
        return context;
    }

}
