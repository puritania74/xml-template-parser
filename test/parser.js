var fs = require('fs');
var path = require('path');

// var expect = require("chai").expect;
var assert = require('chai').assert;
var DOMParser = require('xmldom').DOMParser;
var XMLParser = require("../dist/xml-template-parser").XMLParser;

describe("Parser", function () {

    var domParser = new DOMParser();

    XMLParser.setOptions('DOMParser', domParser);

    describe('Parse XML document', function () {

        var documents = {
            "document3.xml": {
                "document3.template.xml": "document3.result.json"
            },
            "document.xml": {
                "template1.xml": "result1.json",
                "template2.xml": "result2.json",
                "template3.xml": "result3.json"
            },
            "document2.xml": {
                "document2.template.xml": "document2.result.json"
            }
        };

        Object.keys(documents).forEach(function (documentName) {
            var tests = documents[documentName];
            var document = getFileText(documentName);

            Object.keys(tests).forEach(function (templateName) {
                var resultName = tests[templateName];

                it('correctly parse \'' + documentName + '\' by template \'' + templateName + '\'', function () {

                    var template = getFileText(templateName);
                    var json = getFileJSON(resultName);

                    var xmlParser = new XMLParser(template);
                    var data  = xmlParser.parse(document);

                    assert.deepEqual(data, json);
                })
            })
        })
    });

});

function getFileJSON(filename) {
    return JSON.parse(getFileText(filename));
}

function getFileText(filename) {
    return fs.readFileSync(getFilePath(filename), 'utf8');
}

function getFilePath(filename) {
    return path.resolve(__dirname, filename);
}