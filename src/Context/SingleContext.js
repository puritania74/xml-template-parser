import { Context } from './Context';
import { Scope } from '../Scope';

export class SingleContext extends Context {

    /**
     *
     * @param {XMLModelItem} item
     * @param parentScope
     * @param {number} [index]
     */
    createScope( item, parentScope, index ) {

        let scope = new Scope( parentScope );

        this.executeBinding(item, scope)

        return scope;
    }
}


