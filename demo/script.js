let
    docElement = document.getElementById('xml-document'),
    tmplElement = document.getElementById('xml-template'),
    resElement = document.getElementById('response');



document.getElementById('parse').addEventListener('click', () => {
    resElement.disabled = true;

    setTimeout(() => {
        let tmpl = tmplElement.value;
        let doc = docElement.value;
        let xmlParser = new window['xml-template-parser'].XMLParser(tmpl);

        let data = xmlParser.parse(doc);
        resElement.value = JSON.stringify(data, null, 4);

        resElement.disabled = false;
    }, 0);



});




