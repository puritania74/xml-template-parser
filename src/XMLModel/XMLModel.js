import { XMLModelItem } from './XMLModelItem';

export class XMLModel {

    /**
     *
     * @param {Document} xml
     */
    constructor( xml ) {
        this.xml = xml;
        this.map = [];
        this.parse();
    }

    /**
     *
     * @param {Array<string>}path
     * @returns {XMLModelItem}
     */
    findByPath( path ) {
        let nodePath = this.makePath(path);
        let item = null;

        for (let i = 0; i < this.map.length; i = i + 1) {
            if (this.map.path === nodePath) {
                item = this.map.item;
                break;
            }
        }

        return item;
    }


    /**
     *
     * @param {Array<string>}path
     * @returns {Array<XMLModelItem>}
     */
    findAllByPath( path ) {
        return this.map.filter(map => map.path === path).map(map => map.item);
    }

    /**
     *
     * @return {Array<XMLModelItem>}
     */
    getChildren() {
        return this.root.getItems();
    }

    /**
     * @protected
     * @return {XMLModel}
     */
    parse() {
        this.root = new XMLModelItem();
        this.parseNodes( this.xml.childNodes, this.root );
        return this;
    }

    makePath(path) {
        return path.join('/')
    }

    /**
     * @protected
     * @param {NodeList} nodes
     * @param {XMLModelItem} [parent = null]
     * @param {Array<string> [path = []]
     */
    parseNodes( nodes, parent = null, path = [] ) {
        for( let i = 0; i < nodes.length; i = i + 1 ) {
            let node = nodes[ i ];
            let tagName = this.extractTagName(node.nodeName)
            switch( node.nodeType ) {
                case 1:
                    //ELEMENT_NODE
                    let nodePath = path.concat( tagName );
                    let item = new XMLModelItem( tagName );
                    this.parseNodeAttributes( node, item );
                    this.addChild( parent, item, this.makePath(nodePath) );

                    if( node.childNodes && node.childNodes.length > 0 ) {
                        this.parseNodes( node.childNodes, item, nodePath );
                    }
                    break;
                case 3:
                    //TEXT_NODE
                case 4:
                    //CDATA_SECTION_NODE
                    let nodeValue = node.nodeValue;
                    let text = nodeValue.trim();
                    if( text.length > 0 ) {
                        parent.setText( text );
                    }
                    break;
            }
        }
    }

    /**
     * @protected
     * @param {string} nodeName
     * @returns {string}
     */
    extractTagName(nodeName) {
        return nodeName ? nodeName.split(':').pop() : '';
    }

    getRoot() {
        return this.root
    }

    /**
     *
     * @param {XMLModelItem} parent
     * @param {XMLModelItem} item
     * @param {string} path
     */
    addChild( parent, item, path ) {
        parent.addItem( item );
        this.map.push( {
            path: path,
            item: item
        } );
    }

    /**
     * @protected
     * @param {Node} node
     * @param {XMLModelItem} item
     */
    parseNodeAttributes( node, item ) {
        for( let i = 0; i < node.attributes.length; i = i + 1 ) {
            let attribute = node.attributes[ i ];
            item.addAttributes( attribute.name, attribute.value );
        }
    }
}