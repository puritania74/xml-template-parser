import { XMLTemplate } from './XMLTemplate';
import { XMLModel } from './XMLModel';


var optionsXMLParser = {
    "DOMParser": (() => typeof DOMParser === 'undefined' ?  null : DOMParser)()
};

export class XMLParser {

    /**
     * @param {string} template
     */
    constructor( template ) {

        this._template = template;
        this._xmlTemplate = null;


        if (typeof optionsXMLParser.DOMParser === 'function') {
            this.parser = new optionsXMLParser.DOMParser();
        } else {
            this.parser = optionsXMLParser.DOMParser;
        }


    }

    static setOptions(name, value) {
        optionsXMLParser[name] = value;
    }


    /**
     * @protected
     * @return {null|XMLTemplate|*}
     */
    getXmlTemplate() {
        this.initXmlTemplate();
        return this._xmlTemplate;
    }

    /**
     * @protected
     */
    initXmlTemplate() {
        if (!this._xmlTemplate) {
            let xml = this.parseXMLFromString( this._template );
            let model = new XMLModel( xml );
            let xmlTemplate = new XMLTemplate( model );
            xmlTemplate.parse();

            this._xmlTemplate = xmlTemplate;
        }
    }

    /**
     *
     * @param {string} xml
     * @return {Object}
     */
    parse( xml ) {
        let data = null;
        let documentXml = this.parseXMLFromString( xml ),
            xmlTemplate = this.getXmlTemplate();

        if (documentXml && xmlTemplate) {
            let model = new XMLModel( documentXml );
            data = xmlTemplate.run( model );
        }

        return data;
    }


    /**
     *
     * @param {string} xmlText
     * @returns {Document}
     */
    parseXMLFromString( xmlText ) {

        let document = this.parser.parseFromString( xmlText, 'text/xml' );
        if (document) {
            let parseError = document.getElementsByTagName('parsererror');
            if (parseError.length > 0) {
                document = null;
            }
        } else {
            document = null;
        }
        return document;
    }


}