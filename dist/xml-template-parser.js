(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define("xml-template-parser", [], factory);
	else if(typeof exports === 'object')
		exports["xml-template-parser"] = factory();
	else
		root["xml-template-parser"] = factory();
})(this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _XMLParser = __webpack_require__(1);

	Object.defineProperty(exports, 'XMLParser', {
	  enumerable: true,
	  get: function get() {
	    return _XMLParser.XMLParser;
	  }
	});

/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports.XMLParser = undefined;

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	var _XMLTemplate = __webpack_require__(2);

	var _XMLModel = __webpack_require__(11);

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	var optionsXMLParser = {
	    "DOMParser": function () {
	        return typeof DOMParser === 'undefined' ? null : DOMParser;
	    }()
	};

	var XMLParser = exports.XMLParser = function () {

	    /**
	     * @param {string} template
	     */
	    function XMLParser(template) {
	        _classCallCheck(this, XMLParser);

	        this._template = template;
	        this._xmlTemplate = null;

	        if (typeof optionsXMLParser.DOMParser === 'function') {
	            this.parser = new optionsXMLParser.DOMParser();
	        } else {
	            this.parser = optionsXMLParser.DOMParser;
	        }
	    }

	    _createClass(XMLParser, [{
	        key: 'getXmlTemplate',


	        /**
	         * @protected
	         * @return {null|XMLTemplate|*}
	         */
	        value: function getXmlTemplate() {
	            this.initXmlTemplate();
	            return this._xmlTemplate;
	        }

	        /**
	         * @protected
	         */

	    }, {
	        key: 'initXmlTemplate',
	        value: function initXmlTemplate() {
	            if (!this._xmlTemplate) {
	                var xml = this.parseXMLFromString(this._template);
	                var model = new _XMLModel.XMLModel(xml);
	                var xmlTemplate = new _XMLTemplate.XMLTemplate(model);
	                xmlTemplate.parse();

	                this._xmlTemplate = xmlTemplate;
	            }
	        }

	        /**
	         *
	         * @param {string} xml
	         * @return {Object}
	         */

	    }, {
	        key: 'parse',
	        value: function parse(xml) {
	            var data = null;
	            var documentXml = this.parseXMLFromString(xml),
	                xmlTemplate = this.getXmlTemplate();

	            if (documentXml && xmlTemplate) {
	                var model = new _XMLModel.XMLModel(documentXml);
	                data = xmlTemplate.run(model);
	            }

	            return data;
	        }

	        /**
	         *
	         * @param {string} xmlText
	         * @returns {Document}
	         */

	    }, {
	        key: 'parseXMLFromString',
	        value: function parseXMLFromString(xmlText) {

	            var document = this.parser.parseFromString(xmlText, 'text/xml');
	            if (document) {
	                var parseError = document.getElementsByTagName('parsererror');
	                if (parseError.length > 0) {
	                    document = null;
	                }
	            } else {
	                document = null;
	            }
	            return document;
	        }
	    }], [{
	        key: 'setOptions',
	        value: function setOptions(name, value) {
	            optionsXMLParser[name] = value;
	        }
	    }]);

	    return XMLParser;
	}();

/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports.XMLTemplate = undefined;

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	var _Context = __webpack_require__(3);

	var _ContentBinding = __webpack_require__(8);

	var _AttributeBinding = __webpack_require__(10);

	var _Scope = __webpack_require__(6);

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	var XMLTemplate = exports.XMLTemplate = function () {

	    /**
	     *
	     * @param {XMLModel} model
	     */
	    function XMLTemplate(model) {
	        _classCallCheck(this, XMLTemplate);

	        this.model = model;
	    }

	    _createClass(XMLTemplate, [{
	        key: 'parse',
	        value: function parse() {
	            this.context = new _Context.SingleContext();
	            this.scan(this.model.getChildren(), this.context);
	        }

	        /**
	         *
	         * @param {Array<XMLModelItem>} items
	         * @param context
	         * @param path
	         */

	    }, {
	        key: 'scan',
	        value: function scan(items, context, path) {
	            var _this = this;

	            if (typeof path === 'undefined') {
	                path = [];
	            }

	            items.forEach(function (item) {
	                // let currentContext;

	                var currentPath = path.concat(item.getName());

	                //Сканируем атрибуты элемента
	                var itemContext = _this.scanAttributes(item, context, currentPath);

	                //Проверяем биндинг в контенте элемента
	                var propertyName = _this.getBindingProperty(item.getText());
	                if (propertyName !== null) {
	                    //Binding to content
	                    // let currentContext = new SingleContext( context, path );
	                    var binding = new _ContentBinding.ContentBinding(propertyName);
	                    itemContext.registerBinding(binding);
	                    // currentContext.registerBinding( binding );
	                }

	                //Сканируем дочерние элементы
	                _this.scan(item.getItems(), itemContext, currentPath);
	            });
	        }
	    }, {
	        key: 'getBindingProperty',
	        value: function getBindingProperty(text) {
	            var name = null;

	            if (text) {
	                var matches = text.trim().match(/^{{(.*)}}$/);
	                name = matches ? matches[1] : null;
	            }

	            return name;
	        }

	        /**
	         *
	         * @param {XMLModel} documentModel
	         * @returns {Object}
	         */

	    }, {
	        key: 'run',
	        value: function run(documentModel) {
	            var rootScope = new _Scope.Scope();

	            var rootModelItem = documentModel.getChildren()[0];
	            var rootContext = this.context.children[0];
	            scanContext(rootContext, rootScope, rootModelItem);

	            /**
	             *
	             * @param {Context} parentContext
	             * @param {Scope} parentScope
	             * @param {XMLModelItem} modelItem
	             */
	            function scanContext(parentContext, parentScope, modelItem) {

	                parentContext.children.forEach(function (context) {
	                    var modelItems = modelItem.findAllByName(context.getName());

	                    modelItems.forEach(function (item, index) {
	                        var scope = context.createScope(item, parentScope, index);
	                        scanContext(context, scope, item);
	                        context.afterBuildContext(item, scope, index);
	                    });
	                });
	            }

	            return rootScope.getProperties();
	        }

	        /**
	         *
	         * @param {XMLModelItem} item
	         * @param {Context} parent
	         * @param {Array<string>} path
	         */

	    }, {
	        key: 'scanAttributes',
	        value: function scanAttributes(item, parent, path) {
	            var _this2 = this;

	            var attributes = item.getAttributes();
	            var context = void 0;
	            var bindings = [];

	            Object.keys(attributes).forEach(function (attributeName) {

	                if (attributeName === 'for') {
	                    var value = attributes[attributeName];
	                    var regexp = /(.*)\s+in\s+(.*)/i;

	                    var matches = value.match(regexp);

	                    if (matches) {
	                        context = new _Context.MultipleContext(parent, path);

	                        var src = matches[1];
	                        var dst = matches[2];
	                        context.setItemsValue(dst);

	                        src = src.replace(/\s/g, '');

	                        if (matches = src.match(/\((.*),(.*)\)/)) {
	                            //as object
	                            context.setKeyValue(matches[1]);
	                            context.setItemValue(matches[2]);
	                        } else {
	                            //item of Array
	                            context.setItemValue(src);
	                        }
	                    }
	                } else if (attributeName.indexOf('xmlns:') === 0) {} else {
	                    var propertyName = _this2.getBindingProperty(attributes[attributeName]);
	                    if (propertyName) {
	                        bindings.push(new _AttributeBinding.AttributeBinding(attributeName, propertyName));
	                    }
	                }
	            });

	            if (!context) {
	                context = new _Context.SingleContext(parent, path);
	            }

	            bindings.forEach(function (binding) {
	                return context.registerBinding(binding);
	            });

	            context.setName(item.getName());
	            return context;
	        }
	    }]);

	    return XMLTemplate;
	}();

/***/ },
/* 3 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _Context = __webpack_require__(4);

	Object.keys(_Context).forEach(function (key) {
	  if (key === "default" || key === "__esModule") return;
	  Object.defineProperty(exports, key, {
	    enumerable: true,
	    get: function get() {
	      return _Context[key];
	    }
	  });
	});

	var _MultipleContext = __webpack_require__(5);

	Object.keys(_MultipleContext).forEach(function (key) {
	  if (key === "default" || key === "__esModule") return;
	  Object.defineProperty(exports, key, {
	    enumerable: true,
	    get: function get() {
	      return _MultipleContext[key];
	    }
	  });
	});

	var _SingleContext = __webpack_require__(7);

	Object.keys(_SingleContext).forEach(function (key) {
	  if (key === "default" || key === "__esModule") return;
	  Object.defineProperty(exports, key, {
	    enumerable: true,
	    get: function get() {
	      return _SingleContext[key];
	    }
	  });
	});

/***/ },
/* 4 */
/***/ function(module, exports) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	var Context = exports.Context = function () {

	    /**
	     *
	     * @param {Context} [parent = null]
	     * @param {Array<string>|null} [path = null]
	     */
	    function Context() {
	        var parent = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
	        var path = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

	        _classCallCheck(this, Context);

	        this.parent = parent;
	        this.path = path;
	        this.name = null;

	        this.init();
	    }

	    _createClass(Context, [{
	        key: "init",
	        value: function init() {
	            this.bindings = [];
	            this.values = [];

	            if (this.parent) {
	                this.parent.addChild(this);
	            }

	            /**
	             *
	             * @type {Array<Context>}
	             */
	            this.children = [];
	        }

	        /**
	         *
	         * @param {string} name
	         */

	    }, {
	        key: "setName",
	        value: function setName(name) {
	            this.name = name;
	        }

	        /**
	         *
	         * @return {string}
	         */

	    }, {
	        key: "getName",
	        value: function getName() {
	            return this.name;
	        }
	    }, {
	        key: "getValues",
	        value: function getValues() {
	            return this.values;
	        }

	        /**
	         *
	         * @param {boolean} multiple
	         */

	    }, {
	        key: "setIsMultiple",
	        value: function setIsMultiple(multiple) {
	            this.multiple = multiple;
	        }

	        /**
	         *
	         * @return {boolean}
	         */

	    }, {
	        key: "getIsMultiple",
	        value: function getIsMultiple() {
	            return this.multiple;
	        }
	    }, {
	        key: "registerBinding",
	        value: function registerBinding(binding) {
	            this.bindings.push(binding);
	        }
	    }, {
	        key: "registerValue",
	        value: function registerValue(value) {
	            this.values.push(value);
	        }
	    }, {
	        key: "getParent",
	        value: function getParent() {
	            return this.parent;
	        }
	    }, {
	        key: "getPath",
	        value: function getPath() {
	            return this.path;
	        }

	        /**
	         *
	         * @param {Context} child
	         */

	    }, {
	        key: "addChild",
	        value: function addChild(child) {
	            this.children.push(child);
	        }

	        /**
	         *
	         * @param {XMLModelItem} item
	         * @param parentScope
	         * @param {number} [index]
	         */

	    }, {
	        key: "createScope",
	        value: function createScope(item, parentScope, index) {}

	        /**
	         *
	         * @param {XMLModelItem} item
	         * @param {Scope} scope
	         * @param {number} [index]
	         */

	    }, {
	        key: "afterBuildContext",
	        value: function afterBuildContext(item, scope, index) {}

	        /**
	         * @protected
	         * @param {XMLModelItem} item
	         * @param {Scope} scope
	         */

	    }, {
	        key: "executeBinding",
	        value: function executeBinding(item, scope) {
	            this.bindings.forEach(function (binding) {
	                var name = binding.getPropertyName();
	                var value = binding.getValue(item);
	                scope.setProperty(name, value);
	            });
	        }
	    }]);

	    return Context;
	}();

/***/ },
/* 5 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports.MultipleContext = undefined;

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

	var _Context2 = __webpack_require__(4);

	var _Scope = __webpack_require__(6);

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

	var MultipleContext = exports.MultipleContext = function (_Context) {
	    _inherits(MultipleContext, _Context);

	    function MultipleContext() {
	        _classCallCheck(this, MultipleContext);

	        return _possibleConstructorReturn(this, (MultipleContext.__proto__ || Object.getPrototypeOf(MultipleContext)).apply(this, arguments));
	    }

	    _createClass(MultipleContext, [{
	        key: 'init',
	        value: function init() {
	            _get(MultipleContext.prototype.__proto__ || Object.getPrototypeOf(MultipleContext.prototype), 'init', this).call(this);
	        }
	    }, {
	        key: 'setItemsValue',
	        value: function setItemsValue(name) {
	            this._itemsName = name;
	        }
	    }, {
	        key: 'getItemsValue',
	        value: function getItemsValue() {
	            return this._itemsName;
	        }
	    }, {
	        key: 'setKeyValue',
	        value: function setKeyValue(name) {
	            this._keyName = name;
	        }
	    }, {
	        key: 'getKeyValue',
	        value: function getKeyValue() {
	            return this._keyName;
	        }
	    }, {
	        key: 'setItemValue',
	        value: function setItemValue(name) {
	            this._itemName = name;
	        }
	    }, {
	        key: 'getItemValue',
	        value: function getItemValue() {
	            return this._itemName;
	        }

	        /**
	         *
	         * @param {XMLModelItem} item
	         * @param parentScope
	         * @param {number} index
	         */

	    }, {
	        key: 'createScope',
	        value: function createScope(item, parentScope, index) {

	            var keyName = this.getKeyValue();
	            var itemName = this.getItemValue();

	            var scope = new _Scope.Scope(parentScope, [keyName, itemName]);

	            this.executeBinding(item, scope);

	            return scope;
	        }

	        /**
	         *
	         * @param {XMLModelItem} item
	         * @param {Scope} scope
	         * @param {number} index
	         */

	    }, {
	        key: 'afterBuildContext',
	        value: function afterBuildContext(item, scope, index) {
	            //Передать локальные значения итерации в значение переменной
	            var keyName = this.getKeyValue();
	            var itemName = this.getItemValue();
	            var itemsName = this.getItemsValue();

	            var key = void 0;

	            if (typeof keyName === 'undefined' || keyName === null) {
	                key = index;
	            } else {
	                key = scope.getLocalProperty(keyName);
	            }

	            var name = itemsName.concat('.', key);

	            scope.setProperty(name, scope.getLocalProperty(itemName));
	        }
	    }]);

	    return MultipleContext;
	}(_Context2.Context);

/***/ },
/* 6 */
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	var Scope = exports.Scope = function () {

	    /**
	     *
	     * @param {Scope} [parent = null]
	     * @param {Array<string>} [localProperties = []]
	     */
	    function Scope() {
	        var parent = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
	        var localProperties = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];

	        _classCallCheck(this, Scope);

	        /**
	         *
	         * @type {Scope}
	         * @private
	         */
	        this._parent = parent;
	        this._properties = {};
	        this._localProperties = localProperties;
	    }

	    /**
	     *
	     * @return {Scope|null}
	     */


	    _createClass(Scope, [{
	        key: 'getParent',
	        value: function getParent() {
	            return this._parent;
	        }
	    }, {
	        key: 'hasParent',
	        value: function hasParent() {
	            return !!this._parent;
	        }

	        /**
	         *
	         * @param {string} propertyName
	         * @param propertyValue
	         */

	    }, {
	        key: 'setProperty',
	        value: function setProperty(propertyName, propertyValue) {

	            var parts = propertyName.split('.');
	            var baseName = parts.shift();

	            if (this._localProperties.indexOf(baseName) !== -1) {
	                this.setPathValue(this._properties, propertyName, propertyValue);
	            } else {
	                if (this.hasParent()) {
	                    this.getParent().setProperty(propertyName, propertyValue);
	                } else {
	                    //Это корневая зона
	                    this.setPathValue(this._properties, propertyName, propertyValue);
	                }
	            }
	        }

	        /**
	         *
	         * @param {string} propertyName
	         * @returns {*}
	         */

	    }, {
	        key: 'getLocalProperty',
	        value: function getLocalProperty(propertyName) {
	            return this._properties[propertyName];
	        }

	        /**
	         *
	         * @param {Object} target
	         * @param {string} path
	         * @param value
	         */

	    }, {
	        key: 'setPathValue',
	        value: function setPathValue(target, path, value) {
	            var parts = path.split('.');
	            var baseName = parts.shift();

	            if (parts.length > 0) {
	                var val = target[baseName] = target[baseName] || this.createValueContainer(parts[0]);

	                for (var i = 0; i < parts.length; i = i + 1) {
	                    var name = parts[i];
	                    if (i === parts.length - 1) {
	                        val[name] = value;
	                    } else {
	                        val = val[name] = val[name] || this.createValueContainer(parts[i + 1]);
	                    }
	                }
	            } else {
	                target[baseName] = value;
	            }
	        }
	    }, {
	        key: 'getProperties',
	        value: function getProperties() {
	            return this._properties;
	        }

	        /**
	         * @protected
	         * @param name
	         * @return {*}
	         */

	    }, {
	        key: 'createValueContainer',
	        value: function createValueContainer(name) {
	            return (/^\d+$/.test(name) ? [] : {}
	            );
	        }
	    }]);

	    return Scope;
	}();

/***/ },
/* 7 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	        value: true
	});
	exports.SingleContext = undefined;

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	var _Context2 = __webpack_require__(4);

	var _Scope = __webpack_require__(6);

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

	var SingleContext = exports.SingleContext = function (_Context) {
	        _inherits(SingleContext, _Context);

	        function SingleContext() {
	                _classCallCheck(this, SingleContext);

	                return _possibleConstructorReturn(this, (SingleContext.__proto__ || Object.getPrototypeOf(SingleContext)).apply(this, arguments));
	        }

	        _createClass(SingleContext, [{
	                key: 'createScope',


	                /**
	                 *
	                 * @param {XMLModelItem} item
	                 * @param parentScope
	                 * @param {number} [index]
	                 */
	                value: function createScope(item, parentScope, index) {

	                        var scope = new _Scope.Scope(parentScope);

	                        this.executeBinding(item, scope);

	                        return scope;
	                }
	        }]);

	        return SingleContext;
	}(_Context2.Context);

/***/ },
/* 8 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.ContentBinding = undefined;

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	var _Binding2 = __webpack_require__(9);

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

	var ContentBinding = exports.ContentBinding = function (_Binding) {
	  _inherits(ContentBinding, _Binding);

	  function ContentBinding(propertyName) {
	    _classCallCheck(this, ContentBinding);

	    return _possibleConstructorReturn(this, (ContentBinding.__proto__ || Object.getPrototypeOf(ContentBinding)).call(this, propertyName));
	  }

	  /**
	   *
	   * @param {XMLModelItem} item
	   */


	  _createClass(ContentBinding, [{
	    key: 'getValue',
	    value: function getValue(item) {
	      return this.convertValue(item.getText());
	    }
	  }]);

	  return ContentBinding;
	}(_Binding2.Binding);

/***/ },
/* 9 */
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
		value: true
	});

	var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	var Binding = exports.Binding = function () {
		function Binding(name) {
			_classCallCheck(this, Binding);

			var _name$split = name.split(':'),
			    _name$split2 = _slicedToArray(_name$split, 2),
			    propertyName = _name$split2[0],
			    propertyType = _name$split2[1];

			this.propertyName = propertyName;
			if (propertyType) {
				this.propertyType = propertyType.toLowerCase();
			}
		}

		_createClass(Binding, [{
			key: 'getPropertyType',
			value: function getPropertyType() {
				return this.propertyType;
			}

			/**
	  *
	   * @param {XMLModelItem} item
	   */

		}, {
			key: 'getValue',
			value: function getValue(item) {}
		}, {
			key: 'getPropertyName',
			value: function getPropertyName() {
				return this.propertyName;
			}

			/**
	  *
	   * @param {string} value
	   */

		}, {
			key: 'convertValue',
			value: function convertValue(value) {
				var val = value;
				if (value !== null && typeof value !== 'undefined' && this.propertyType) {

					switch (this.propertyType) {
						case 'boolean':
							var regexp = /^(true|yes|t|y|1)$/i;
							val = regexp.test(value);
							break;
						case 'int':
							val = parseInt(value, 10);
							break;
						case 'float':
							val = parseFloat(value);
							break;
					}
				}

				return val;
			}
		}]);

		return Binding;
	}();

/***/ },
/* 10 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.AttributeBinding = undefined;

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	var _Binding2 = __webpack_require__(9);

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

	var AttributeBinding = exports.AttributeBinding = function (_Binding) {
	  _inherits(AttributeBinding, _Binding);

	  function AttributeBinding(attributeName, propertyName) {
	    _classCallCheck(this, AttributeBinding);

	    var _this = _possibleConstructorReturn(this, (AttributeBinding.__proto__ || Object.getPrototypeOf(AttributeBinding)).call(this, propertyName));

	    _this.attributeName = attributeName;
	    return _this;
	  }

	  /**
	   *
	   * @param {XMLModelItem} item
	   */


	  _createClass(AttributeBinding, [{
	    key: 'getValue',
	    value: function getValue(item) {
	      var attributes = item.getAttributes();
	      return this.convertValue(attributes[this.attributeName]);
	    }
	  }]);

	  return AttributeBinding;
	}(_Binding2.Binding);

/***/ },
/* 11 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _XMLModel = __webpack_require__(12);

	Object.keys(_XMLModel).forEach(function (key) {
	  if (key === "default" || key === "__esModule") return;
	  Object.defineProperty(exports, key, {
	    enumerable: true,
	    get: function get() {
	      return _XMLModel[key];
	    }
	  });
	});

	var _XMLModelItem = __webpack_require__(13);

	Object.keys(_XMLModelItem).forEach(function (key) {
	  if (key === "default" || key === "__esModule") return;
	  Object.defineProperty(exports, key, {
	    enumerable: true,
	    get: function get() {
	      return _XMLModelItem[key];
	    }
	  });
	});

/***/ },
/* 12 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports.XMLModel = undefined;

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	var _XMLModelItem = __webpack_require__(13);

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	var XMLModel = exports.XMLModel = function () {

	    /**
	     *
	     * @param {Document} xml
	     */
	    function XMLModel(xml) {
	        _classCallCheck(this, XMLModel);

	        this.xml = xml;
	        this.map = [];
	        this.parse();
	    }

	    /**
	     *
	     * @param {Array<string>}path
	     * @returns {XMLModelItem}
	     */


	    _createClass(XMLModel, [{
	        key: 'findByPath',
	        value: function findByPath(path) {
	            var nodePath = this.makePath(path);
	            var item = null;

	            for (var i = 0; i < this.map.length; i = i + 1) {
	                if (this.map.path === nodePath) {
	                    item = this.map.item;
	                    break;
	                }
	            }

	            return item;
	        }

	        /**
	         *
	         * @param {Array<string>}path
	         * @returns {Array<XMLModelItem>}
	         */

	    }, {
	        key: 'findAllByPath',
	        value: function findAllByPath(path) {
	            return this.map.filter(function (map) {
	                return map.path === path;
	            }).map(function (map) {
	                return map.item;
	            });
	        }

	        /**
	         *
	         * @return {Array<XMLModelItem>}
	         */

	    }, {
	        key: 'getChildren',
	        value: function getChildren() {
	            return this.root.getItems();
	        }

	        /**
	         * @protected
	         * @return {XMLModel}
	         */

	    }, {
	        key: 'parse',
	        value: function parse() {
	            this.root = new _XMLModelItem.XMLModelItem();
	            this.parseNodes(this.xml.childNodes, this.root);
	            return this;
	        }
	    }, {
	        key: 'makePath',
	        value: function makePath(path) {
	            return path.join('/');
	        }

	        /**
	         * @protected
	         * @param {NodeList} nodes
	         * @param {XMLModelItem} [parent = null]
	         * @param {Array<string> [path = []]
	         */

	    }, {
	        key: 'parseNodes',
	        value: function parseNodes(nodes) {
	            var parent = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
	            var path = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : [];

	            for (var i = 0; i < nodes.length; i = i + 1) {
	                var node = nodes[i];
	                var tagName = this.extractTagName(node.nodeName);
	                switch (node.nodeType) {
	                    case 1:
	                        //ELEMENT_NODE
	                        var nodePath = path.concat(tagName);
	                        var item = new _XMLModelItem.XMLModelItem(tagName);
	                        this.parseNodeAttributes(node, item);
	                        this.addChild(parent, item, this.makePath(nodePath));

	                        if (node.childNodes && node.childNodes.length > 0) {
	                            this.parseNodes(node.childNodes, item, nodePath);
	                        }
	                        break;
	                    case 3:
	                    //TEXT_NODE
	                    case 4:
	                        //CDATA_SECTION_NODE
	                        var nodeValue = node.nodeValue;
	                        var text = nodeValue.trim();
	                        if (text.length > 0) {
	                            parent.setText(text);
	                        }
	                        break;
	                }
	            }
	        }

	        /**
	         * @protected
	         * @param {string} nodeName
	         * @returns {string}
	         */

	    }, {
	        key: 'extractTagName',
	        value: function extractTagName(nodeName) {
	            return nodeName ? nodeName.split(':').pop() : '';
	        }
	    }, {
	        key: 'getRoot',
	        value: function getRoot() {
	            return this.root;
	        }

	        /**
	         *
	         * @param {XMLModelItem} parent
	         * @param {XMLModelItem} item
	         * @param {string} path
	         */

	    }, {
	        key: 'addChild',
	        value: function addChild(parent, item, path) {
	            parent.addItem(item);
	            this.map.push({
	                path: path,
	                item: item
	            });
	        }

	        /**
	         * @protected
	         * @param {Node} node
	         * @param {XMLModelItem} item
	         */

	    }, {
	        key: 'parseNodeAttributes',
	        value: function parseNodeAttributes(node, item) {
	            for (var i = 0; i < node.attributes.length; i = i + 1) {
	                var attribute = node.attributes[i];
	                item.addAttributes(attribute.name, attribute.value);
	            }
	        }
	    }]);

	    return XMLModel;
	}();

/***/ },
/* 13 */
/***/ function(module, exports) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	var XMLModelItem = exports.XMLModelItem = function () {
	    function XMLModelItem(name) {
	        _classCallCheck(this, XMLModelItem);

	        /**
	         *
	         * @type {Array<XMLModelItem>}
	         */
	        this._items = [];
	        this._attributes = {};
	        this._text = null;
	        this._name = name;
	    }

	    _createClass(XMLModelItem, [{
	        key: "setText",
	        value: function setText(text) {
	            this._text = text;
	        }
	    }, {
	        key: "getText",
	        value: function getText() {
	            return this._text;
	        }
	    }, {
	        key: "getItems",
	        value: function getItems() {
	            return this._items;
	        }
	    }, {
	        key: "getName",
	        value: function getName() {
	            return this._name;
	        }

	        /**
	         *
	         * @param {XMLModelItem} item
	         */

	    }, {
	        key: "addItem",
	        value: function addItem(item) {
	            this._items.push(item);
	        }
	    }, {
	        key: "addAttributes",
	        value: function addAttributes(name, value) {
	            this._attributes[name] = value;
	        }

	        /**
	         *
	         * @param {string} name
	         * @returns {Array<XMLModelItem>}
	         */

	    }, {
	        key: "findAllByName",
	        value: function findAllByName(name) {
	            return this.getItems().filter(function (item) {
	                return item.getName() === name;
	            });
	        }
	    }, {
	        key: "getAttributes",
	        value: function getAttributes() {
	            return this._attributes;
	        }
	    }]);

	    return XMLModelItem;
	}();

/***/ }
/******/ ])
});
;