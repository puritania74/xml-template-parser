export class XMLModelItem {


    constructor(name) {
        /**
         *
         * @type {Array<XMLModelItem>}
         */
        this._items = [];
        this._attributes = {};
        this._text = null;
        this._name = name;
    }

    setText(text) {
        this._text = text;
    }

    getText() {
        return this._text;
    }

    getItems() {
        return this._items;
    }

    getName() {
        return this._name;
    }

    /**
     *
     * @param {XMLModelItem} item
     */
    addItem(item) {
        this._items.push(item);
    }

    addAttributes(name, value) {
        this._attributes[name] = value;
    }


    /**
     *
     * @param {string} name
     * @returns {Array<XMLModelItem>}
     */
    findAllByName(name) {
        return this.getItems().filter(item => item.getName() === name);
    }

    getAttributes() {
        return this._attributes;
    }


}