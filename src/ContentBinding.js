import {Binding} from './Binding';

export class ContentBinding extends Binding {

	constructor(propertyName) {
		super(propertyName);		
	}

    /**
     *
     * @param {XMLModelItem} item
     */
    getValue(item) {
        return this.convertValue(item.getText());
    }

}
