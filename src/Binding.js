export class Binding {


	constructor(name) {
		let [propertyName, propertyType] = name.split(':');

		this.propertyName = propertyName;
		if (propertyType) {
            this.propertyType = propertyType.toLowerCase();
		}

	}

	getPropertyType() {
		return this.propertyType;
	}

    /**
	 *
     * @param {XMLModelItem} item
     */
	getValue(item) {

	}

	getPropertyName() {
		return this.propertyName;
	}

    /**
	 *
     * @param {string} value
     */
	convertValue(value) {
		let val = value;
		if (value !== null && typeof value !== 'undefined' && this.propertyType) {

            switch(this.propertyType) {
				case 'boolean':
					let regexp = /^(true|yes|t|y|1)$/i;
                    val = regexp.test(value);
                    break;
				case 'int':
					val = parseInt(value, 10);
					break;
				case 'float':
					val = parseFloat(value);
					break;
            }
		}

		return val;
	}
}
