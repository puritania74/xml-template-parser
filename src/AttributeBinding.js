import {Binding} from './Binding';

export class AttributeBinding extends Binding {

	constructor(attributeName, propertyName) {
			super(propertyName);

			this.attributeName = attributeName;
	}

    /**
     *
     * @param {XMLModelItem} item
     */
    getValue(item) {
		let attributes = item.getAttributes();
		return this.convertValue(attributes[this.attributeName]);
    }

}
