export class Context {

    /**
     *
     * @param {Context} [parent = null]
     * @param {Array<string>|null} [path = null]
     */
    constructor( parent = null, path = null ) {
        this.parent = parent;
        this.path = path;
        this.name = null;

        this.init();
    }

    init() {
        this.bindings = [];
        this.values = [];

        if( this.parent ) {
            this.parent.addChild( this );
        }

        /**
         *
         * @type {Array<Context>}
         */
        this.children = [];
    }

    /**
     *
     * @param {string} name
     */
    setName(name) {
        this.name = name;
    }

    /**
     *
     * @return {string}
     */
    getName() {
        return this.name;
    }

    getValues() {
        return this.values;
    }

    /**
     *
     * @param {boolean} multiple
     */
    setIsMultiple(multiple) {
        this.multiple = multiple;
    }

    /**
     *
     * @return {boolean}
     */
    getIsMultiple() {
        return this.multiple;
    }

    registerBinding( binding ) {
        this.bindings.push( binding );
    }

    registerValue( value ) {
        this.values.push( value );
    }

    getParent() {
        return this.parent;
    }

    getPath() {
        return this.path;
    }

    /**
     *
     * @param {Context} child
     */
    addChild( child ) {
        this.children.push( child );
    }

    /**
     *
     * @param {XMLModelItem} item
     * @param parentScope
     * @param {number} [index]
     */
    createScope( item, parentScope, index ) {

    }

    /**
     *
     * @param {XMLModelItem} item
     * @param {Scope} scope
     * @param {number} [index]
     */
    afterBuildContext(item, scope, index) {

    }

    /**
     * @protected
     * @param {XMLModelItem} item
     * @param {Scope} scope
     */
    executeBinding(item, scope) {
        this.bindings.forEach( binding => {
            let name = binding.getPropertyName();
            let value = binding.getValue(item);
            scope.setProperty( name, value );
        } );
    }

}
